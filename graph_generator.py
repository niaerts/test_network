import numpy as np

from random import Random
from scipy.spatial import Delaunay

def random_graph(nodes_count, X_border, Y_border, convert_int=True, graph=None):
    names, pos, connections, distance = None, None, None, None
    
    if graph:
        pos = graph["pos"]
        connections = graph["connections"]
        names = graph["names"]
        distance = graph["distance"]
    else:
        pos = np.zeros((nodes_count, 2))
        connections = {}
        names = np.arange(0, nodes_count).tolist()

    r = Random()

    i = 0

    distance = np.zeros((nodes_count,nodes_count))
    
    while i < nodes_count:
        x, y = r.random() * X_border, r.random() * Y_border
        
        if convert_int:
            x, y = int(x), int(y)

        pos_i = np.array([x, y])

        distance[i, :i] = np.sqrt(np.sum(np.power(pos[:i] - pos_i, 2), axis=1))
        
        if np.sum(distance[i, :i] < 1.0) == 0:
            pos[i] = pos_i

            connections[i] = set()

            i += 1
            
    distance += np.triu(distance.T, 1)
    
    return {
        "pos": pos,
        "connections": connections,
        "names": names,
        "distance": distance,
        "labels": {}
    }

def gabriel(graph):
    names, pos, connections, distance = graph["names"], graph["pos"], graph["connections"], graph["distance"]
    
    for i in range(len(names)):
        for j in range(i + 1, len(names)):
            d = distance[i, j]

            in_b = distance[i, :] <= d

            in_b[i] = False
            in_b[j] = False

            in_range = np.where(in_b)

            intersect = False
            if in_range[0].shape[0] > 0:
                in_range_pos = pos[in_range]

                centerpoint = (pos[i] + pos[j]) / 2.0

                in_range_dist = np.sqrt(np.sum(np.power(in_range_pos - centerpoint, 2), axis=1))

                intersect = np.sum(in_range_dist <= d / 2) != 0

            if not intersect:
                connections[i].add(j)
                connections[j].add(i)
    
    return graph

def delaunay(graph):
    names, pos, connections, distance = graph["names"], graph["pos"], graph["connections"], graph["distance"]
    
    tri = Delaunay(pos)

    def connect(n1, n2):
        connections[n1].add(n2)
        connections[n2].add(n1)

    for i in range(tri.simplices.shape[0]):
        a, b, c = tri.simplices[i,0], tri.simplices[i,1], tri.simplices[i,2]
        connect(a,b)
        connect(b,c)
        connect(c,a)

    return graph