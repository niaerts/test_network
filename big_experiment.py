import pickle
from functools import partial

import pygraphviz as pgv

import network_analyzer
from data_collector import process_oping_logs
from network_builder import load_network
from node_info import cache_nodes_info
from rumba import log
from rumba.model import *
from rumba.storyboard import Client, StoryBoard, Server, Event
from rumba.utils import ExperimentManager, PAUSE_SWAPOUT

# import testbed plugins
import rumba.testbeds.dockertb as docker
import rumba.testbeds.jfed as jfed
from rumba.executors.docker import DockerException
from rumba.ssh_support import SSHException
from rumba.testbeds import local

# import prototype plugins
import rumba.prototypes.ouroboros as our

description = "Script to create an ECMP network"

log.set_logging_level('DEBUG')
logger = log.get_logger(__name__)

f = open("big_experiment.pkl", "rb")

data = pickle.loads(f.read())

f.close()

isp_upstream = data["isp_upstream"]
print(isp_upstream)
client_upstream = data["client_upstream"]

NODES_COUNT = len(client_upstream)

privacy_connections = data["privacy_connections"]

global_net, private_net, nodes, shims, b_nodes, b_shims, isp_details, isp_upstream, c_nodes, c_shims, c_upstream = load_network(NODES_COUNT, isp_upstream, client_upstream)

nodes = [] + b_nodes
shims = [] + b_shims

for isp in isp_details.values():
    nodes += isp["nodes"]
    shims += isp["shims"]

nodes += c_nodes
shims += c_shims

print("Total nodes required: %i" % len(nodes))

EthDIFS = [layer for layer in shims if isinstance(layer, ShimEthDIF)]

global_graph = pgv.AGraph()
global_graph.read("global_network.gv")
global_graph.has_layout = True

privacy_graph = pgv.AGraph()
privacy_graph.read("privacy_network.gv")
privacy_graph.has_layout = True

tb = docker.Testbed("ouroboros", pull_image=False, use_ovs=True)

# tb = local.Testbed("ecmp_test", "niaerts")

# tb = jfed.Testbed("opriv2", "niaerts", "/home/nick/.ssh/jfed.pem",
#                   authority="exogeni.net:slvmsite", proj_name="Ouroboros",
#                   exp_hours="1")

# tb = jfed.Testbed("opriv", "niaerts", "/home/nick/.ssh/jfed.pem",
#                   authority="wall2.ilabt.iminds.be", proj_name="Ouroboros",
#                   exp_hours="1")
#
# for node in nodes:
#     node.machine_type = "virtual"

exp = our.Experiment(tb, nodes=nodes)

storyboard = StoryBoard(duration=30, experiment=exp)

servers = [c_nodes[0]]

starttime = 10
oping_servers = []
for server in servers:
    oping_client = Client("oping", options="-c 1000 -n %s.oping -i 25 -s 1024" %
                                           server.name)

    oping_server = Server("oping", 0.2, 5, options="-l")

    oping_server.clients = [oping_client]

    oping_servers.append(oping_server)

    storyboard.add_server_on_node(oping_server, server)

    for node in c_nodes:
        if node != server:
            action = partial(storyboard.run_client, oping_client, 30,
                             node=node)
            storyboard.add_event(Event(action, ev_time=starttime))
            starttime += 31

            save_stats_action = partial(
                network_analyzer.retrieve_bandwidth_stats, global_graph, "global",
                nodes, shims
            )
            storyboard.add_event(Event(save_stats_action, ev_time=starttime))

            save_private_stats_action = partial(
                network_analyzer.retrieve_layer_bandwidth_stats, privacy_graph,
                "private", private_net, nodes,
            )
            storyboard.add_event(Event(save_private_stats_action, ev_time=starttime+1))

            starttime += 14

            oping_client.add_node(node)

storyboard.duration = starttime

def load_private_dt(exp):
    nodes_ids = {}

    i=0
    for node in c_nodes:
        nodes_ids[node] = i
        i+=1

    r = 0
    for i in range(len(exp.dt_flows)):
        connected_nodes = set()
        connected_nodes.add(c_nodes[0])
        todo = []
        dt_flow = exp.dt_flows[i]

        flows = []
        if len(dt_flow) and dt_flow[0]["src"].dif == private_net:
            for flow in dt_flow:
                sid = nodes_ids[flow["src"].node]
                did = nodes_ids[flow["dst"].node]
                if (sid, did) in privacy_connections or (did, sid) in privacy_connections:
                    todo.append(flow)
                else:
                    print("Removed %s" % flow)
                    r += 1

            while len(todo) > 0:
                flow = todo.pop(0)
                if flow["src"].node in connected_nodes or flow["dst"].node in connected_nodes:
                    flows.append(flow)
                    connected_nodes.add(flow["src"].node)
                    connected_nodes.add(flow["dst"].node)
                else:
                    todo.append(flow)

            exp.dt_flows[i] = flows
    print("Removed %i flows from private graph" % r)

def init(exp):
    if not isinstance(tb, docker.Testbed):
        exp.install_prototype()

    exp.bootstrap_prototype()

swapin_succes = False

while not swapin_succes:
    with ExperimentManager(exp, swap_out_strategy=PAUSE_SWAPOUT):
        load_private_dt(exp)

        exp.swap_in()

        swapin_succes = True

        init(exp)

        for server in servers:
            server.execute_commands([
                "irm r n %s.oping l private" % server.name,
                "irm b p oping n %s.oping" % server.name
            ])

        cache_nodes_info(nodes)

        network_analyzer.retrieve_bandwidth_stats(global_graph, "global", nodes,
                                                  shims)
        network_analyzer.retrieve_layer_bandwidth_stats(privacy_graph, "private",
                                                        private_net, nodes)

        storyboard.start()

        process_oping_logs(c_nodes)