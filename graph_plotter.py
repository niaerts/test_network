import pygraphviz as pgv

from nxpd import nxpdParams, draw

nxpdParams['show'] = 'ipynb'

def plot_graph(graph, filename=None, only_labeled=False):
    names, pos, connections, labels = graph["names"], graph["pos"], graph["connections"], graph["labels"]
    
    directed=only_labeled
    
    G = pgv.AGraph(directed=directed)

    for i in range(len(names)):
        G.add_node(names[i])

        for j in connections[i]:
            label = None
            if labels and i in labels and j in labels[i]:
                label = labels[i][j]
                
                if isinstance(label, float):
                    label = "{0:.2f}".format(label)
            
            if not only_labeled:
                G.add_edge(names[i], names[j])
            elif label:
                G.add_edge(names[i], names[j])
                
                E = G.get_edge(names[i], names[j])
                E.attr["label"] = label
                E.attr["dir"] = "forward"

        node = G.get_node(names[i])
        node.attr["pos"] = str(pos[i, 0] * 2)+","+str(pos[i,1] * 2)+"!"
        node.attr["style"] = "filled"

    G.layout()
    
    if filename:
        G.draw(filename)
    
    G.draw("test.svg")
    return G