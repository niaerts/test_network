#!/usr/bin/env python

# Script to create a network with four nodes in a circle, used to test ECMP implementation

from time import sleep

from rumba.model import *
from rumba.storyboard import Client, StoryBoard, Server
from rumba.utils import ExperimentManager

# import testbed plugins
import rumba.testbeds.qemu as qemu
import rumba.testbeds.docker_testbed as docker
import rumba.testbeds.jfed as jfed

# import prototype plugins
import rumba.prototypes.ouroboros as our

import argparse
import sys


description = "Script to create an ECMP network"

log.set_logging_level('DEBUG')

n01 = NormalDIF("n01")

n01.add_policy("routing", "iimr")
n01.add_policy("pff", "mpi")

shims = []
nodes = []

for i in range(0,7):
    shims.append(ShimEthDIF("e" + str(i)))

for i in range(0, 5):
    if i == 0:
        node = Node("node" + str(i), difs = [n01, shims[i], shims[4]], dif_registrations = {n01 : [shims[i], shims[4]]})
    else:
        node = Node("node" + str(i), difs=[n01, shims[i-1], shims[i]], dif_registrations={n01: [ shims[i-1], shims[i] ]})

    nodes.append(node)

nodes[2].add_dif(shims[5])
nodes[2].add_dif_registration(n01, shims[5])

nodes[0].add_dif(shims[6])
nodes[0].add_dif_registration(n01, shims[6])

nodes.append(Node("node5", difs = [n01, shims[5]], dif_registrations= {n01 : [ shims[5] ]}))
nodes.append(Node("node6", difs = [n01, shims[6]], dif_registrations= {n01 : [ shims[6] ]}))

tb = docker.Testbed("ouroboros", pull_image=False, use_ovs=True)

exp = our.Experiment(tb, nodes = nodes)

storyboard = StoryBoard(duration=30, experiment=exp)

oping_client = Client("oping", options="-c 5 -n oping.server")

oping_server = Server("oping", 0.2, 2, options="-l")

oping_server.clients = [oping_client]

storyboard.add_server_on_node(oping_server, nodes[0])

oping_client.add_node(nodes[2])

print(exp)

with ExperimentManager(exp):
    exp.swap_in()
    exp.bootstrap_prototype()

    nodes[0].execute_commands([
        "irm reg name oping.server layer %s" % n01.name,
        "irm bind program oping name oping.server"
    ])

    # storyboard.start()

    input("Press Enter to clean up experiment")


    storyboard.fetch_logs()
