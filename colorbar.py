import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import FuncFormatter

from plot import percent
import network_analyzer

x_range = np.arange(0 , 1, 1.0 / 20) + (1.0 / 20) / 2
y_range = np.ones(20)

colors = [network_analyzer.get_color(c, 1.0) for c in x_range]

fig = plt.figure(figsize=(4,1))

ax = fig.add_subplot(1,1,1)

ax.yaxis.set_visible(False)
ax.xaxis.set_major_formatter(FuncFormatter(percent))

ax.spines['top'].set_visible(False)
ax.spines['left'].set_visible(False)
ax.spines['right'].set_visible(False)

ax.bar(x_range, y_range, 1.0 / 20, color=colors)
ax.set_xlim(left=0, right=1.0)
plt.xticks(np.arange(0, 1.005, step=0.5))
plt.tight_layout()

plt.savefig("colorbar.pdf")