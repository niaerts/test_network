import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plot
from matplotlib.ticker import FuncFormatter
import os, math
import numpy as np

import config

def percent(v, _):
    return str(100 * v) + ' %'

def milliseconds(v, _):
    return "%i ms" % v


def _hostname_path_create(hostname):
    f = os.path.join(config.RESULTS_FOLDER, hostname)

    if not os.path.exists(f):
        os.makedirs(f)

    return f


def plot_latency(hostname, client, data):
    plot.clf()

    plot.gca().yaxis.set_major_formatter(FuncFormatter(milliseconds))

    plot.boxplot(data["latency"], labels=[
        "%s --> %s" % (client.name, hostname)
    ])

    f = os.path.join(_hostname_path_create(hostname), "%s.pdf" % client.name)

    plot.savefig(f)


def plot_agg(name, hostname, agg, max_x):
    plot.clf()

    plot.gca().yaxis.set_major_formatter(FuncFormatter(percent))
    plot.gca().xaxis.set_major_formatter(FuncFormatter(milliseconds))
    plot.ylim(0, 1)

    bins = np.arange(0, max_x, math.ceil(max_x / 20))

    h, b = np.histogram(agg[hostname], bins=bins)

    plot.bar(b[:-1], h.astype(np.float32) / h.sum(), width=b[1]-b[0])

    f = os.path.join(_hostname_path_create(hostname), "%s_hist.pdf" % name)

    plot.savefig(f)