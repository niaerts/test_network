import os

import math
import pygraphviz as pgv
from colour import Color

from node_info import get_node_info, get_node_from_address
from rumba.elements.topology import NormalDIF, ShimEthDIF

import config
import data_collector

def _graph_path_create():
    f = config.RESULTS_FOLDER

    if not os.path.exists(f):
        os.makedirs(f)

    return f


def create_graph(name, nodes, shims):
    connections = {}
    for layer in shims:
        if not len(layer.members) == 2:
            raise ValueError("Only expecting layers with 2 members (PTP)")

        connections[layer.members[0].name, layer.members[1].name] = layer.name

    return create_graph_connections(name, connections)


color_range = list(Color("DarkGreen").range_to(Color("DarkRed"), 20))
multiplier = float(len(color_range))


def get_color(value, max):
    color = color_range[math.floor(multiplier * value / (max)) if value < max else math.floor(multiplier - 1)]

    return color.get_hex()


def get_penwidth(value, max):
    if max == 0:
        return "1.0"

    r = (4.0 * float(value) / (0.5 * max)) + 1
    return str(r)


def create_graph_connections(name, connections):
    G = pgv.AGraph(directed=False)

    G.graph_attr["overlap"] = False
    G.graph_attr["splines"] = True
    G.edge_attr["len"] = 2

    for c, label in connections.items():
        m1, m2 = c

        if m1 not in G.nodes():
            G.add_node(m1)

        if m2 not in G.nodes():
            G.add_node(m2)

        G.add_edge(m1, m2)

        E = G.get_edge(m1, m2)

        E.attr["label"] = label

    path = os.path.join(_graph_path_create(), "%s.pdf" % name)

    G.layout()

    G.draw(path)

    return G


_labels = ["", "Ki", "Mi", "Gi"]


def human_format_bandwidth(b):
    if b == 0:
        return "0.0 B"

    l = int(math.log(b, 1024))
    s = int(math.pow(1024, l))

    b /= s

    return "%.1f %sB" % (b, _labels[l])


def create_bandwidth_graph(G, name, bandwidth, src=None, dst=None):
    node_bandwith = {}

    for k, v in bandwidth.items():
        m0, m1 = k

        if m0 in node_bandwith:
            node_bandwith[m0] += v
        else:
            node_bandwith[m0] = v

        if m1 in node_bandwith:
            node_bandwith[m1] += v
        else:
            node_bandwith[m1] = v

    if src is not None and dst is not None:
        max_b = max(node_bandwith[src], node_bandwith[dst])
    else:
        max_b = config.TOTAL_SESSION_SIZE

    for k, v in bandwidth.items():
        m0, m1 = k
        E = G.get_edge(m0.name, m1.name)

        E.attr["color"] = get_color(v, max_b)
        E.attr["penwidth"] = get_penwidth(v, max_b)
        # if v > math.pow(10,5):
        # E.attr["label"] = human_format_bandwidth(v)
        # else:
        #     E.attr["label"] = ""

    path = os.path.join(_graph_path_create(), "%s_network_bandwith.pdf" % name)

    for n, v in node_bandwith.items():
        N = G.get_node(n.name)

        # N.attr["label"] = human_format_bandwidth(v)
        N.attr["fillcolor"] = get_color(v, 2 * max_b) # * 2 due to egress and ingress
        N.attr["color"] = "white"
        N.attr["fontcolor"] = "white"

    if src:
        print("Source: %s" % src.name)
        G.get_node(src.name).attr["fillcolor"] = Color("DarkBlue").get_hex()

    if dst:
        G.get_node(dst.name).attr["fillcolor"] = Color("DarkBlue").get_hex()

    G.draw(path)

    if src:
        del G.get_node(src.name).attr["fillcolor"]

    if dst:
        del G.get_node(dst.name).attr["fillcolor"]


def find_upper_layer(node, lower):
    for u, l in node.dif_registrations.items():
        for l_d in l:
            if l_d == lower:
                return u

    return None


def _diff_bandwidth(bandwidth, previous_bandwidth):
    r = {}

    for k in bandwidth.keys():
        r[k] = bandwidth[k] - previous_bandwidth[k]

    return r


_previous_bandwidth = {}
_counter = {}


def retrieve_bandwidth_stats(graph, name, nodes, shims, src=None, dst=None):
    sent = {}

    for node in nodes:
        for dif in node.difs:
            if not isinstance(dif, NormalDIF):
                continue

            fds = get_node_info(node)[dif]["fds"]
            pid = get_node_info(node)[dif]["pid"]
            address = get_node_info(node)[dif]["address"]

            for fd in fds:
                stats = data_collector.get_stats(node, pid, fd)

                remote = stats["Endpoint address"]

                sent[address, remote] = stats["Qos cube   0"]["sent (bytes)"]

    bandwidth = {}
    for shim in shims:
        if not isinstance(shim, ShimEthDIF):
            continue

        l = find_upper_layer(shim.members[0], shim) # Should be the same

        a0 = get_node_info(shim.members[0])[l]["address"]
        a1 = get_node_info(shim.members[1])[l]["address"]

        bandwidth[(shim.members[0], shim.members[1])] = int(sent[a0, a1]) + int(sent[a1, a0])

    if name not in _counter:
        _counter[name] = 0

    _name = "%s_%i" % (name, _counter[name])
    _counter[name] += 1

    if name not in _previous_bandwidth:

        create_bandwidth_graph(graph, _name, bandwidth, src, dst)
    else:
        create_bandwidth_graph(
            graph, _name, _diff_bandwidth(bandwidth, _previous_bandwidth[name])
        )

    _previous_bandwidth[name] = bandwidth


_previous_bandwidth_layer = {}


def retrieve_layer_bandwidth_stats(graph, name, layer, nodes, src=None, dst=None):
    bandwidth = {}

    for node in layer.members:
        fds = get_node_info(node)[layer]["fds"]
        pid = get_node_info(node)[layer]["pid"]

        for fd in fds:
            stats = data_collector.get_stats(node, pid, fd)

            remote = get_node_from_address(stats["Endpoint address"])

            if (remote, node) in bandwidth:
                bandwidth[remote, node] += int(stats["Qos cube   0"]["sent (bytes)"])
            else:
                bandwidth[node, remote] = int(stats["Qos cube   0"]["sent (bytes)"])

    if name not in _counter:
        _counter[name] = 0

    _name = "%s_%i" % (name, _counter[name])
    _counter[name] += 1

    if name not in _previous_bandwidth_layer:
        create_bandwidth_graph(graph, _name, bandwidth, src, dst)
    else:
        create_bandwidth_graph(
            graph, _name, _diff_bandwidth(bandwidth, _previous_bandwidth_layer[name]), src, dst
        )

    _previous_bandwidth_layer[name] = bandwidth
