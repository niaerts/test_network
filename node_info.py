import data_collector

_node_info = {}
_address_map = {}

_logs = {}


def _get_syslog_sanitized(node, cache=True):
    def retrieve_syslog(node):
        log_file = data_collector.read_remote_file(node, "/var/log/syslog")
        log_lines = data_collector.sanitize_lines(log_file.splitlines())

        return log_lines

    if node not in _logs or not cache:
        _logs[node] = retrieve_syslog(node)

    return _logs[node]


def _clear_syslog_cache():
    _logs.clear()


def cache_nodes_info(nodes):
    for node in nodes:
        if node in _node_info:
            continue

        _node_info[node] = {}
        for layer in node.difs:
            pid = data_collector.get_pid(_get_syslog_sanitized(node), layer)
            address = data_collector.get_address(_get_syslog_sanitized(node), pid)
            fds = data_collector.get_fds(_get_syslog_sanitized(node), pid)

            _address_map[address] = node
            _node_info[node][layer] = {
                "pid": pid,
                "address": address,
                "fds": fds
            }


def get_node_info(node):
    if node not in _node_info:
        cache_nodes_info([node])

    return _node_info[node]


def get_node_from_address(address):
    if address in _address_map:
        return _address_map[address]
    else:
        return None