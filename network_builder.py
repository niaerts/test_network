import json
import random

from rumba.elements.topology import LinkQuality, NormalDIF, ShimEthDIF, Node

lq1 = LinkQuality(delay=1)
lq5 = LinkQuality(delay=5)

isps = [
    ("isp/small_isp1.json", "isp1", "eu"),
    ("isp/small_isp2.json", "isp2", "eu"),
    ("isp/small_isp3.json", "isp3", "eu"),
    ("isp/small_isp4.json", "isp4", "us"),
    ("isp/small_isp5.json", "isp5", "us")
]

def find_node(nodes, name):
    for node in nodes:
        if name == node.name:
            return node

    print("Could not find node with name: %s" % name)


def build_backbone(global_net):
    n = NormalDIF("b")

    # n.add_policy("routing", "ecmp")
    # n.add_policy("pff", "mp")

    shims_eu = []
    shims_us = []
    nodes_eu = []
    nodes_us = []

    for nodes, shims, prefix in [(nodes_eu, shims_eu, "eu"),
                                 (nodes_us, shims_us, "us")]:
        for i in range(3):
            shims.append(ShimEthDIF("%se%i" % (prefix, i), link_quality=lq5))

        for i in range(3):
            if i == 0:
                nodes.append(Node("%snode%i" % (prefix, i),
                            difs=[n, global_net, shims[i], shims[2]],
                            dif_registrations={n: [shims[i], shims[2]],
                                               global_net: [n]}))
            else:
                nodes.append(Node("%snode%i" % (prefix, i),
                            difs=[n, global_net, shims[i-1], shims[i]],
                            dif_registrations={n: [shims[i-1], shims[i]],
                                               global_net: [n]}))

    lq = LinkQuality(40)
    shims_sub = [ShimEthDIF("sube%i" % i, link_quality=lq) for i in range(2)]

    for (i, j) in [(0, 0), (2, 1)]:
        nodes_eu[i].add_dif(shims_sub[j])
        nodes_us[i].add_dif(shims_sub[j])

        nodes_eu[i].add_dif_registration(n, shims_sub[j])
        nodes_us[i].add_dif_registration(n, shims_sub[j])

    nodes = nodes_eu + nodes_us
    shims = shims_eu + shims_us + shims_sub

    return n, nodes, shims


def build_isp(isp_name, isp_file, upstream, global_net):
    f = open(isp_file, "r")

    r = json.loads(f.read())

    f.close()

    n = NormalDIF(isp_name)

    # n.add_policy("routing", "ecmp")
    # n.add_policy("pff", "mp")

    nodes = [Node("%snode%i" % (isp_name, i), difs=[n],
                  dif_registrations={n: []}) for i in range(len(r))]

    shims = {}

    for i in range(len(r)):
        for j in r[i]:
            if (i, j) in shims or (j, i) in shims:
                continue
            else:
                shim = ShimEthDIF("%se%i" % (isp_name, len(shims)), link_quality=lq1)

                nodes[i].add_dif(shim)
                nodes[j].add_dif(shim)

                nodes[i].add_dif_registration(n, shim)
                nodes[j].add_dif_registration(n, shim)

                shims[i, j] = shim

    main = nodes[0]

    uplink = ShimEthDIF("uplink%se%i" % (isp_name, 0), link_quality=lq1)

    main.add_dif(uplink)
    main.add_dif(global_net)

    upstream.add_dif(uplink)

    main.add_dif_registration(global_net, n)

    main.add_dif_registration(global_net, uplink)
    upstream.add_dif_registration(global_net, uplink)

    shims = list(set(shims.values()))

    shims.append(uplink)

    return n, nodes, shims


def build_clients(count, global_net, private_net, isps=None, upstream=None):
    if not (isps is None) ^ (upstream is None):
        raise ValueError("build_clients needs either isps or upstreams")

    nodes = []
    shims = []

    isp_layers = set()
    upstream_isp_layer = {}
    if not upstream:
        upstream = []

        isp_names = list(isps.keys())
        for i in range(count):
            isp_name = random.choice(isp_names)

            isp = isps[isp_name]
            isp_nodes = isp["nodes"]

            upstream.append(random.choice(isp_nodes))

            upstream_isp_layer[upstream[i]] = isp["layer"]
    else:
        for node in upstream:
            for dif in node.difs:
                if dif.name.startswith("isp"):
                    isp_layers.add(dif)

        for node in upstream:
            if node in upstream_isp_layer:
                continue

            for dif in node.difs:
                if dif in isp_layers:
                    upstream_isp_layer[node] = dif
                    break

    for i in range(count):

        shim = ShimEthDIF("cliente%i" % i, link_quality=lq1)

        upstream[i].add_dif(shim)

        if global_net not in upstream[i].difs:
            upstream[i].add_dif(global_net)
            upstream[i].add_dif_registration(global_net, upstream_isp_layer[upstream[i]])

        upstream[i].add_dif_registration(global_net, shim)

        nodes.append(Node("client%i" % i, difs=[shim, global_net, private_net],
                          dif_registrations={global_net: [shim],
                                             private_net: [global_net]}))

        shims.append(shim)

    upstream = [node.name for node in upstream]

    return nodes, shims, upstream

def random_network(CLIENT_NODES):
    global_net = NormalDIF("global")

    private_net = NormalDIF("private")
    # private_net.add_policy("routing", "iimr")
    # private_net.add_policy("pff", "mpi")

    nodes = []
    shims = []

    b_n, b_nodes, b_shims = build_backbone(global_net)

    nodes += b_nodes
    shims += b_shims

    upstream_nodes = {
        "us": [node for node in b_nodes if node.name.startswith("us")],
        "eu": [node for node in b_nodes if node.name.startswith("eu")]
    }

    isp_details = {}
    isp_upstream = []
    for (isp_file, isp_name, continent) in isps:
        upstream_node = random.choice(upstream_nodes[continent])
        i_n, i_nodes, i_shims = build_isp(isp_name,
                                          isp_file,
                                          upstream_node,
                                          global_net)

        isp_details[isp_name] = {
            "layer": i_n,
            "nodes": i_nodes,
            "shims": i_shims
        }

        nodes += i_nodes
        shims += i_shims

        isp_upstream.append(upstream_node.name)

    c_nodes, c_shims, c_upstream = build_clients(CLIENT_NODES, global_net, private_net, isp_details)

    # c_nodes, c_shims, c_upstream = build_clients(CLIENT_NODES, global_net, private_net,
    #                                  isps={"global": {"layer": b_n, "nodes": b_nodes}})

    nodes += c_nodes
    shims += c_shims

    return global_net, private_net, nodes, shims, b_nodes, b_shims, isp_details, isp_upstream, c_nodes, c_shims, c_upstream


def load_network(CLIENT_NODES, isp_upstream, client_upstream):
    global_net = NormalDIF("global")
    
    private_net = NormalDIF("private")
    # private_net.add_policy("routing", "iimr")
    # private_net.add_policy("pff", "mpi")

    nodes = []
    shims = []

    b_n, b_nodes, b_shims = build_backbone(global_net)

    nodes += b_nodes
    shims += b_shims

    upstream_nodes = {
        "us": [node for node in b_nodes if node.name.startswith("us")],
        "eu": [node for node in b_nodes if node.name.startswith("eu")]
    }

    isp_details = {}
    isp_upstream = [find_node(nodes, name) for name in isp_upstream]

    i=0
    for (isp_file, isp_name, continent) in isps:
        i_n, i_nodes, i_shims = build_isp(isp_name,
                                          isp_file,
                                          isp_upstream[i],
                                          global_net)

        isp_details[isp_name] = {
            "layer": i_n,
            "nodes": i_nodes,
            "shims": i_shims
        }

        nodes += i_nodes
        shims += i_shims
        i += 1

    c_upstream = [find_node(nodes, name) for name in client_upstream]

    c_nodes, c_shims, c_upstream = build_clients(CLIENT_NODES, global_net, private_net,
                                                 upstream=c_upstream)

    # c_nodes, c_shims, c_upstream = build_clients(CLIENT_NODES, global_net, private_net,
    #                                  isps={"global": {"layer": b_n, "nodes": b_nodes}})

    nodes += c_nodes
    shims += c_shims

    return global_net, private_net, nodes, shims, b_nodes, b_shims, isp_details, isp_upstream, c_nodes, c_shims, c_upstream