# Script to create a network with four nodes in a circle, used to test ECMP implementation

from time import sleep

import data_collector
from rumba import log
from rumba.model import *
from rumba.storyboard import Client, StoryBoard, Server
from rumba.utils import ExperimentManager

# import testbed plugins
import rumba.testbeds.dockertb as docker
import rumba.testbeds.jfed as jfed
import rumba.testbeds.local as local

# import prototype plugins
import rumba.prototypes.ouroboros as our

description = "Script to create an ECMP network"

log.set_logging_level('DEBUG')
logger = log.get_logger(__name__)

n01 = NormalDIF("n01")

n01.add_policy("routing", "ecmp")
n01.add_policy("pff", "mp")

shims = []
nodes = []

link_q25 = LinkQuality(delay=Delay(25))

for i in range(0,4):
    shim = ShimEthDIF("e" + str(i))

    shim.link_quality = link_q25

    shims.append(shim)

for i in range(0, 4):
    if i == 0:
        node = Node("node" + str(i), difs = [n01, shims[i], shims[3]], dif_registrations = {n01 : [shims[i], shims[3]]})
    else:
        node = Node("node" + str(i), difs=[n01, shims[i-1], shims[i]], dif_registrations={n01: [ shims[i-1], shims[i] ]})

    nodes.append(node)

tb = docker.Testbed("ecmp_test", "ouroboros",
                    pull_image=False, use_ovs=True)

# tb = local.Testbed("test", "niaerts")

# tb = jfed.Testbed("ecmptest", "niaerts",
#                   cert_file="/home/nick/.ssh/jfed.pem",
#                   proj_name="Ouroboros", authority="exogeni.net")

exp = our.Experiment(tb, nodes = nodes,
                     git_repo="https://gitlab.com/niaerts/ouroboros",
                     git_branch="iimr-s")

storyboard = StoryBoard(duration=30, experiment=exp)

oping_client = Client("oping", options="-c 5 -n oping.server")

oping_server = Server("oping", 0.2, 2, options="-l")

oping_server.clients = [oping_client]

storyboard.add_server_on_node(oping_server, nodes[0])

oping_client.add_node(nodes[2])

print(exp)

with ExperimentManager(exp):
    exp.swap_in()
    # exp.install_prototype()
    exp.bootstrap_prototype()

    nodes[0].execute_commands([
        "irm reg name oping.server layer %s" % n01.name,
        "irm bind program oping name oping.server"
    ])

    storyboard.start()

    sleep(5)

    link_q10 = LinkQuality(delay=Delay(10, jitter=2, correlation=25.0,
                                       distribution=Distribution.NORMAL),
                           loss=Loss(0.1),
                           rate=1)

    for shim in shims:
        shim.link_quality = link_q10

    sleep(5)

    for shim in shims:
        del shim.link_quality

    sleep(5)

    link_r1 = LinkQuality(rate=1)

    for shim in shims:
        shim.link_quality = link_r1

    input("Press Enter to clean up experiment")

    stats = {}
    n_addresses = {}
    a_nodes = {}

    for node in nodes:
        log_file = data_collector.read_remote_file(node, "/var/log/syslog")
        pid = data_collector.get_pid(log_file, n01)

        address = data_collector.get_address(log_file, pid)

        n_addresses[node.name] = address
        a_nodes[address] = node.name

        fds = data_collector.get_fds(log_file, pid)

        for fd in fds:
            print(data_collector.get_stats(node, pid, fd)["Qos cube   0"]["sent (packets)"])

    storyboard.fetch_logs()
