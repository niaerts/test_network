from subprocess import check_call

from os import listdir, remove

from os.path import isfile

routing_mode = ["iimr", "link_state", "ecmp"]
forwarding_mode = ["mpi", "simple", "mp"]

commands = []
retry_counters = {}
for delay_arg in ["long", "short"]:
    for i in range(len(routing_mode)):
        commands.append("python3 ./big_experiment_private_only.py %s %s %s" % (delay_arg, routing_mode[i], forwarding_mode[i]))

while len(commands):
    command = commands.pop(0)

    if check_call(command.split()):
        if command not in retry_counters:
            retry_counters[command] = 1
        else:
            retry_counters[command] = retry_counters[command] + 1

            if retry_counters[command] < 3:
                commands.append(command)

    for f in listdir("/var/run/netns"):
        if isfile(f):
            remove(f)

