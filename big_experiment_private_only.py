import pickle
import random
import traceback
from functools import partial
from time import sleep

import sys

import math

import config
import network_analyzer
from data_collector import process_oping_logs
from network_builder import load_network
from node_info import cache_nodes_info
from rumba import log
from rumba.model import *
from rumba.storyboard import Client, StoryBoard, Server, Event
from rumba.utils import ExperimentManager, PAUSE_SWAPOUT
import pygraphviz as pgv

# import testbed plugins
import rumba.testbeds.dockertb as docker
import rumba.testbeds.jfed as jfed
from rumba.executors.docker import DockerException
from rumba.ssh_support import SSHException
from rumba.testbeds import local

# import prototype plugins
import rumba.prototypes.ouroboros as our

description = "Script to create an ECMP network"

log.set_logging_level('DEBUG')
logger = log.get_logger(__name__)

delay_arg = sys.argv[1]
routing_arg = sys.argv[2]
forwarding_arg = sys.argv[3]

f = open("big_experiment.pkl", "rb")

data = pickle.loads(f.read())

f.close()

isp_upstream = data["isp_upstream"]
client_upstream = data["client_upstream"]
privacy_delays = data["privacy_delays_long"] if delay_arg == "long" else data["privacy_delays_short"]
servers = data["servers"]
clients = data["clients"]

NODES_COUNT = len(client_upstream)

privacy_connections = data["privacy_connections"]

privacy_graph = pgv.AGraph()
privacy_graph.read("privacy_network.gv")
privacy_graph.has_layout = True

private_net = NormalDIF("private")

nodes = []
for i in range(NODES_COUNT):
    node = Node("client%i" % (i))

    node.add_dif(private_net)

    nodes.append(node)


server_nodes = []

for server in servers:
    for node in nodes:
        if node.name == server:
            server_nodes.append(node)

client_nodes = []

for client in clients:
    for node in nodes:
        if node.name == client:
            client_nodes.append(node)

shims = []
for (i, j) in privacy_connections:
    shim = ShimEthDIF("shim%i" % len(shims))

    n1 = nodes[i].name
    n2 = nodes[j].name

    delay = privacy_delays[n1, n2] if (n1, n2) in privacy_delays else privacy_delays[n2, n1]

    shim.link_quality = LinkQuality(delay=delay)

    nodes[i].add_dif(shim)
    nodes[j].add_dif(shim)

    nodes[i].add_dif_registration(private_net, shim)
    nodes[j].add_dif_registration(private_net, shim)

    shims.append(shim)


print("Total nodes required: %i" % len(nodes))

EthDIFS = [layer for layer in shims if isinstance(layer, ShimEthDIF)]

tb = docker.Testbed("ouroboros", pull_image=False, use_ovs=True)

# tb = local.Testbed("ecmp_test", "niaerts")

# tb = jfed.Testbed("opriv2", "niaerts", "/home/nick/.ssh/jfed.pem",
#                   authority="exogeni.net:slvmsite", proj_name="Ouroboros",
#                   exp_hours="1")

# tb = jfed.Testbed("opriv", "niaerts", "/home/nick/.ssh/jfed.pem",
#                   authority="wall2.ilabt.iminds.be", proj_name="Ouroboros",
#                   exp_hours="1")
#
# for node in nodes:
#     node.machine_type = "virtual"

exp = our.Experiment(tb, nodes=nodes)

storyboard = StoryBoard(duration=30, experiment=exp)

starttime = 30
oping_servers = []
id = 0

def oping_kill(node):
    try:
        node.execute_command("pkill oping")
    except DockerException:
        pass


def run_client_oping(server, client, ctr):
    # oping_kill(server)

    client.execute_command("oping -c %i -n %s.oping -i %i -s %i > /tmp/oping_%i.rumba.log"
                           % (config.TOTAL_PACKETS, server.name, config.INTERVAL,
                           config.PACKET_SIZE, ctr))

    # oping_kill(server)
    ctr += 1

    network_analyzer.retrieve_layer_bandwidth_stats(privacy_graph, "private", private_net, nodes, src=client, dst=server)


# for server in server_nodes:
#     oping_client = Client("oping", options="-c %i -n %s.oping -i %i -s %i" %
#                           (config.TOTAL_PACKETS, server.name, config.INTERVAL,
#                            config.PACKET_SIZE), shutdown="")
#
#     oping_server = Server("oping", 0.2, 5, options="-l", s_id="oping_%i" % id)
#
#     id += 1
#
#     oping_server.clients = [oping_client]
#
#     oping_servers.append(oping_server)
#
#     storyboard.add_server_on_node(oping_server, server)
#
#     for node in client_nodes:
#         duration = 20
#         action = partial(storyboard.run_client, oping_client, duration,
#                          node=node)
#         storyboard.add_event(Event(action, ev_time=starttime))
#         starttime += duration
#
#         # save_stats_action = partial(
#         #     network_analyzer.retrieve_bandwidth_stats, global_graph, "global",
#         #     nodes, shims
#         # )
#         # storyboard.add_event(Event(save_stats_action, ev_time=starttime))
#
#         save_private_stats_action = partial(
#             network_analyzer.retrieve_layer_bandwidth_stats, privacy_graph,
#             "private", private_net, nodes, src=node, dst=server
#         )
#         storyboard.add_event(Event(save_private_stats_action, ev_time=starttime))
#
#         starttime += 10
#
#         oping_client.add_node(node)

storyboard.duration = 1 # starttime

def init(exp):
    if not isinstance(tb, docker.Testbed):
        exp.install_prototype()

    exp.bootstrap_prototype()

private_net.add_policy("routing", routing_arg)
private_net.add_policy("pff", forwarding_arg)

config.RESULTS_FOLDER = "results_%s_%s" % (delay_arg, routing_arg)

with ExperimentManager(exp):
    exp.swap_in()

    init(exp)

    storyboard.start()

    for server in server_nodes:
        server.execute_commands([
            "irm r n %s.oping l private" % server.name,
            "irm b p oping n %s.oping" % server.name,
            "./startup.sh oping_server oping -l"
        ])

    cache_nodes_info(nodes)

    network_analyzer.retrieve_layer_bandwidth_stats(privacy_graph, "private",
                                                    private_net, nodes)

    ctr = 0
    for client in client_nodes:
        for server in server_nodes:
            run_client_oping(server, client, ctr)
            ctr += 1

    if delay_arg == "short":
        process_oping_logs(nodes, 200, 80)
    else:
        process_oping_logs(nodes, 400, 150)
    input()
    exp.swap_out()
