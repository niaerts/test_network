import os
import shutil
import pickle
import math
import networkx as nx
import matplotlib.pyplot as plt

import graph_generator
import graph_plotter
from network_analyzer import create_graph
from rumba import log

from network_builder import *

CLIENT_NODES = 10

log.set_logging_level('DEBUG')
logger = log.get_logger(__name__)

if os.path.exists("results"):
    shutil.rmtree("results")

global_net, private_net, nodes, shims, b_nodes, b_shims, isp_details, isp_upstream, c_nodes, c_shims, client_upstream = random_network(CLIENT_NODES)

print("Total nodes required: %i" % len(nodes))

EthDIFS = [layer for layer in shims if isinstance(layer, ShimEthDIF)]

G = nx.Graph()

delays = {}
for node in nodes:
    G.add_node(node.name)

for shim in EthDIFS:
    n1 = shim.members[0].name
    n2 = shim.members[1].name

    delays[n1, n2] = shim.link_quality.delay.delay
    delays[n2, n1] = shim.link_quality.delay.delay
    G.add_edge(n1, n2, weight=1)

graph = create_graph("global_network", nodes, EthDIFS)

graph.write("global_network.gv")

_s = int(math.sqrt(3 * CLIENT_NODES)) + 1

p_g = graph_generator.random_graph(CLIENT_NODES, _s, _s)

for i in range(len(p_g["names"])):
    p_g["names"][i] = "client%i" % i

graph_generator.delaunay(p_g)

def get_delay(n1, n2, max_link_delay=None):
    path = nx.dijkstra_path(G, n1, n2)

    delay = 0
    while len(path) > 1:
        c = path.pop(0)
        p = path[0]

        if max_link_delay:
            delay += min(delays[c, p], max_link_delay)
        else:
            delay += delays[c, p]

    return delay


c = set()
privacy_delays_long = {}
privacy_delays_short = {}
for n1, l in p_g["connections"].items():
    for n2 in l:
        if (n2, n1) not in c:
            privacy_delays_long[c_nodes[n1].name, c_nodes[n2].name] = get_delay(c_nodes[n1].name, c_nodes[n2].name)
            privacy_delays_short[c_nodes[n1].name, c_nodes[n2].name] = get_delay(c_nodes[n1].name, c_nodes[n2].name, max_link_delay=5)

            c.add((n1, n2))

c = list(c)

privacy_graph = graph_plotter.plot_graph(p_g, "results/privacy_network.pdf")

privacy_graph.write("privacy_network.gv")

f = open("big_experiment.pkl", "wb")

servers = [c_nodes[i] for i in range(0,3)]

client_candidates = [node.name for node in (set(c_nodes) - set(servers))]
clients = random.sample(client_candidates, min(10, len(client_candidates)))

servers = [server.name for server in servers]

f.write(pickle.dumps({
    "client_upstream": client_upstream,
    "isp_upstream": isp_upstream,
    "privacy_connections": c,
    "privacy_delays_long": privacy_delays_long,
    "privacy_delays_short": privacy_delays_short,
    "clients": clients,
    "servers": servers
}))

f.close()
