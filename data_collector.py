import re
from subprocess import CalledProcessError

from plot import plot_latency, plot_agg
from rumba.executors.docker import DockerException
from rumba.model import Node, DIF
import rumba.log as log

import pandas as pd
import numpy as np

from rumba.ssh_support import SSHException

logger = log.get_logger(__name__)

NORMAL_PID_REGEX = [
    "Bootstrapped IPCP ([0-9]+) in layer %s\.",
    "Enrolled IPCP ([0-9]+) in layer %s\."
    ]

ADDRESS_REGEX = "IPCP got address ([0-9]+)\."
DT_FDS_REGEX = "Added fd ([0-9]+) to SDU scheduler\."

OPING_REGEX = ("(?P<size>[0-9]+) bytes from (?P<hostname>.*): seq=[0-9]+ time="
               "(?P<latency>[0-9]+.[0-9]+) ms")


START_REGEX = "Ouroboros IPC Resource Manager daemon started..."

class DataCollectorException(Exception):
    pass


def read_remote_file(node, path):
    return node.execute_command("cat %s" % path, as_root=True)


def sanitize_lines(lines):
    last = 0

    for i in range(len(lines)):
        line = lines[i]

        if START_REGEX in line:
            last = i

    return lines[last:]


def get_pid(lines, layer):
    m = None

    for line in lines:
        for r in NORMAL_PID_REGEX:
            m = re.search(r % layer.name, line)

            if m:
                return m.group(1)

    return None


def get_pid_logs(lines, pid):
    pid_stdout = "==%s==" % pid.zfill(5)
    pid_syslog = "[%s]:" % pid

    return [line for line in lines if line.startswith(pid_stdout)
            or pid_syslog in line]


def get_address(lines, pid=None, layer=None):
    if not (pid or layer):
        logger.error("Get_address needs a pid or a layer!")
        raise DataCollectorException("Get_address needs a pid or a layer!")

    if not pid:
        pid = get_pid(log, layer)

    for line in get_pid_logs(lines, pid):
        r = re.search(ADDRESS_REGEX, line)

        if r:
            return r.group(1)


def get_fds(lines, pid=None, layer=None):
    if not (pid or layer):
        logger.error("Get_address needs a pid or a layer!")
        raise DataCollectorException("Get_address needs a pid or a layer!")

    if not pid:
        pid = get_pid(lines, layer)

    fds = []
    for line in get_pid_logs(lines, pid):
        r = re.search(DT_FDS_REGEX, line)

        if r:
            fds.append(r.group(1))

    return fds


def parse_stats(lines, spaces=0):
    d = {}

    while len(lines):
        line = lines[0]

        if not re.match(" {%i}.*" % spaces, line):
            return d

        lines.pop(0)

        line = line.strip()

        if re.match(".*:.*", line):
            head, tail = line.split(":", 1)

            if len(tail) == 0:
                d[head] = parse_stats(lines, spaces+1)
            else:
                d[head] = tail.strip()

    return d


def parse_oping_logs(lines):
    res = {}

    for line in lines:
        r = re.search(OPING_REGEX, line)

        if r:
            d = r.groupdict()

            hostname = d["hostname"]
            size = int(d["size"])
            latency = float(d["latency"])

            if hostname not in res:
                res[hostname] = {"size": [], "latency": []}

            res[hostname]["size"].append(size)
            res[hostname]["latency"].append(latency)

    for hostname in res.keys():
        res[hostname] = pd.DataFrame.from_dict(res[hostname])

    return res


def aggregate_latency(agg, hostname, latency):
    if hostname not in agg:
        agg[hostname] = []

    agg[hostname] += list(latency["latency"])


def process_oping_logs(nodes, max_delay, max_jitter):
    agg = {}
    agg_jitter = {}

    for node in nodes:
        try:
            files = node.execute_command("ls /tmp/oping_*.rumba.log").splitlines()

            for file in files:
                out = read_remote_file(node, file)

                latency = parse_oping_logs(out.splitlines())

                for hostname in latency.keys():
                    aggregate_latency(agg, hostname, latency[hostname])

                    jitter = np.abs(np.diff(agg[hostname]))

                    if hostname not in agg_jitter:
                        agg_jitter[hostname] = []

                    agg_jitter[hostname] += jitter.flatten().tolist()

                    plot_latency(hostname, node, latency[hostname])

        except(SSHException, DockerException, CalledProcessError):
            print("Error whilst reading oping logs on %s" % node.name)
    for hostname in agg.keys():
        plot_agg("latency", hostname, agg, max_delay)
        plot_agg("jitter", hostname, agg_jitter, max_jitter)


def get_stats(node, pid, fd):
    path = "/tmp/ouroboros/ipcpd-normal.%s/dt/%s" % (pid, fd)

    log_file = read_remote_file(node, path)

    return parse_stats(log_file.splitlines())
