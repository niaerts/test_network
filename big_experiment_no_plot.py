import pickle
from functools import partial

from network_builder import load_network
from node_info import cache_nodes_info
from rumba import log
from rumba.model import *
from rumba.storyboard import Client, StoryBoard, Server, Event
from rumba.utils import ExperimentManager, PAUSE_SWAPOUT

# import testbed plugins
import rumba.testbeds.dockertb as docker
import rumba.testbeds.jfed as jfed
from rumba.executors.docker import DockerException
from rumba.ssh_support import SSHException
from rumba.testbeds import local

# import prototype plugins
import rumba.prototypes.ouroboros as our

description = "Script to create an ECMP network"

log.set_logging_level('DEBUG')
logger = log.get_logger(__name__)

f = open("big_experiment.pkl", "rb")

data = pickle.loads(f.read())

f.close()

isp_upstream = data["isp_upstream"]
print(isp_upstream)
client_upstream = data["client_upstream"]

NODES_COUNT = len(client_upstream)

privacy_connections = data["privacy_connections"]

global_net, private_net, nodes, shims, b_nodes, b_shims, isp_details, isp_upstream, c_nodes, c_shims, c_upstream = load_network(NODES_COUNT, isp_upstream, client_upstream)

nodes = [] + b_nodes
shims = [] + b_shims

for isp in isp_details.values():
    nodes += isp["nodes"]
    shims += isp["shims"]

nodes += c_nodes
shims += c_shims

print("Total nodes required: %i" % len(nodes))

EthDIFS = [layer for layer in shims if isinstance(layer, ShimEthDIF)]

# tb = docker.Testbed("ouroboros", pull_image=False, use_ovs=True)

tb = local.Testbed("ecmp_test", "niaerts")

# tb = jfed.Testbed("opriv2", "niaerts", "/home/nick/.ssh/jfed.pem",
#                   authority="exogeni.net:slvmsite", proj_name="Ouroboros",
#                   exp_hours="1")

# tb = jfed.Testbed("opriv", "niaerts", "/home/nick/.ssh/jfed.pem",
#                   authority="wall2.ilabt.iminds.be", proj_name="Ouroboros",
#                   exp_hours="1")
#
# for node in nodes:
#     node.machine_type = "virtual"

exp = our.Experiment(tb, nodes=nodes)

servers = [c_nodes[0]]

def load_private_dt(exp):
    nodes_ids = {}

    i=0
    for node in c_nodes:
        nodes_ids[node] = i
        i+=1

    r = 0
    for i in range(len(exp.dt_flows)):
        connected_nodes = set()
        connected_nodes.add(c_nodes[0])
        todo = []
        dt_flow = exp.dt_flows[i]

        flows = []
        if len(dt_flow) and dt_flow[0]["src"].dif == private_net:
            for flow in dt_flow:
                sid = nodes_ids[flow["src"].node]
                did = nodes_ids[flow["dst"].node]
                if (sid, did) in privacy_connections or (did, sid) in privacy_connections:
                    todo.append(flow)
                else:
                    print("Removed %s" % flow)
                    r += 1

            while len(todo) > 0:
                flow = todo.pop(0)
                if flow["src"].node in connected_nodes or flow["dst"].node in connected_nodes:
                    flows.append(flow)
                    connected_nodes.add(flow["src"].node)
                    connected_nodes.add(flow["dst"].node)
                else:
                    todo.append(flow)

            exp.dt_flows[i] = flows
    print("Removed %i flows from private graph" % r)

def init(exp):
    if not isinstance(tb, docker.Testbed):
        exp.install_prototype()

    exp.bootstrap_prototype()

with ExperimentManager(exp, swap_out_strategy=PAUSE_SWAPOUT):
    load_private_dt(exp)

    exp.swap_in()

    init(exp)

    for server in servers:
        server.execute_commands([
            "irm r n %s.oping l private" % server.name,
            "irm b p oping n %s.oping" % server.name
        ])

    cache_nodes_info(nodes)